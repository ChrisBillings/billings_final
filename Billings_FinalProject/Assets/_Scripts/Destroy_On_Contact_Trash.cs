﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy_On_Contact_Trash : MonoBehaviour
{

    private GameController gameController;
    public int scoreValue;

    // Start is called before the first frame update
    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Net")
        {
            gameController.AddScore(scoreValue);
            Destroy(gameObject);
        }
        if(other.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
}
