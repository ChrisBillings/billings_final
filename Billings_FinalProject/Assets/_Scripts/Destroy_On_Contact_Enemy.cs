﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy_On_Contact_Enemy : MonoBehaviour
{
    private GameController gameController;
    public GameObject[] pickupPrefabs;
    GameObject Pickup;
   public Patrol patrol;




    // Start is called before the first frame update
    void Start()
    {
        
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }


        patrol = GetComponent<Patrol>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {if(other.tag == "Bullet")
        {
            SpawnPickup(other);
            Destroy(gameObject);
        }
    
     if(other.tag == "Player")
        {
            Destroy(other.gameObject);
            gameController.GameOver();
            gameController.HealthDown();
        }
     
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.otherCollider.tag == "Enemy1")
        {
            patrol.Turn();
        }
        
    
    }
    void SpawnPickup(Collider2D other)
    {
        GameObject pickupToSpawn = pickupPrefabs[(int)(Random.value * pickupPrefabs.Length)];
        Pickup = Instantiate(pickupToSpawn, other.transform.position, pickupToSpawn.transform.rotation);
        //makes pickups 
    }
}
