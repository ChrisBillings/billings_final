﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    public float speed;
    private bool movingRight = true;
    public Transform groundDetection;

    void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);//moves the boi

        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, 10f);
        if(groundInfo.collider == false)
        {
            Turn();
        }
    }
    public void Turn()
    {
        if (movingRight == true)
        {
            transform.eulerAngles = new Vector3(0, -180, 0);//flips the boi around
            movingRight = false;
        }
        else
        {
            transform.eulerAngles = new Vector3(0, 0, 0);//flip him bcak right 
            movingRight = true;
        }
    }
}
