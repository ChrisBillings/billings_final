﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{

    public int speed;

    //private Rigidbody2D rb;

    //public float tumble;

    void Start()
    {
        transform.Rotate(new Vector3(-90, 0, 0));
        // rb = GetComponent<Rigidbody2D>();

        // rb.angularVelocity = Random.insideUnitSphere * tumble;
    }
    private void Update()
    {
        
        transform.Rotate(new Vector3(0, 0, 50) * speed * Time.deltaTime);
    }
}
