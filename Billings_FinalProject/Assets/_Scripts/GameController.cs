﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    public static GameController gc;


    public Text scoreText;
    public int score;
    public Text healthText;
    public int health;
    public Text restartText;
    public Text gameOverText;

    private bool gameOver;
    private bool restart;

    // Start is called before the first frame update
    void Awake()
    {

        if (gc == null)
        {
            gc = this;
            DontDestroyOnLoad(gameObject);


            SetStartValues();
            HealthDown();

        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    void SetStartValues()
    {
        score = 0;
        UpdateScore();
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
    }

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {


        GameObject refObj = GameObject.FindGameObjectWithTag("UIRef");
        UI_References use = refObj.GetComponent<UI_References>();
        restartText = use.restartText;
        scoreText = use.scoreText;
        healthText = use.healthText;
        gameOverText = use.gameOverText;
        //print(health);
        healthText.text = "Attempts Left: " + health;
        scoreText.text = "Trash: " + score;

        SetStartValues();
    }


    // Update is called once per frame
    void Update()
    {
        if (gameOver)
        {
        if (health >= 0)
            {
                restartText.text = "Press 'R' to restart";
                restart = true;
            }
         
        }
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }
    }
    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }
    void UpdateScore()
    {
        scoreText.text = "Trash: " + score;
    }
    public void HealthDown()
    {
        health -= 1;
        healthText.text = "Attempts Left: " + health;
    }
    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }
}
