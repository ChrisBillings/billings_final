﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement_Script : MonoBehaviour
{
    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;

    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck;

    public GameObject net;
    public GameObject bullet;
    public Transform shotSpawns;
    public Transform bulletSpawns;

    public bool grounded = false;
    private bool Bullet = true;
    //private Animator anim;
    private Rigidbody2D rb2d;

    // Start is called before the first frame update
    void Awake()
    {
        //anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Bullet = true;
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Bullet = false;
        }
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        if (Input.GetButtonDown("Jump") && grounded)
        {
            jump = true;
        }
        if(Input.GetButtonDown("Fire1"))
        {
            if(Bullet == false)
            {
                Instantiate(net, shotSpawns.position, shotSpawns.rotation);
            }
            else
            {
                Instantiate(bullet, bulletSpawns.position, bulletSpawns.rotation);
            }
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        if(facingRight == true)
        {
            shotSpawns.transform.rotation = Quaternion.Euler(Vector3.zero);
        }
        else
        {
            shotSpawns.transform.rotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));
        }
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        //anim.SetFloat("Speed", Mathf.Abs(h));

        if (h * rb2d.velocity.x < maxSpeed)
            rb2d.AddForce(Vector2.right * h * moveForce);

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();

        if (jump)
        {
            //anim.SetTrigger("Jump");
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }
    }
}
